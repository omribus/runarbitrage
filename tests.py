from config import ConfigRopsten
from src.arbitrage_manager import ArbitrageManager
from src.contract_api import ContractApi
from src.logger import Logger

from time import sleep, time

logger = Logger()


class Test:
    class TestSmartContracts:
        @staticmethod
        def test_uniswap_smart_contract():
            uniswap_exchange_contract = ContractApi(ConfigRopsten.ContractPath.UNISWAP_EXCHANGE)
            uniswap_exchange_contract.deploy(provided_address=ConfigRopsten.EthereumAddress.UNISWAP_ADDRESS_ROUTER,
                                             deposit_on_deploy=1000)
            logger.log("Waiting 2 seconds for contracts to finish deploying....")
            sleep(2)

            ETH = uniswap_exchange_contract.create_valid_address(ConfigRopsten.EthereumAddress.UNISWAP_ADDRESS_ETH)
            DAI = uniswap_exchange_contract.create_valid_address(ConfigRopsten.EthereumAddress.UNISWAP_ADDRESS_DAI)
            amount_in = 500
            amount_out_min = 0

            uniswap_exchange_contract.call_method('readBalance')
            uniswap_exchange_contract.call_method('buyTokens', deadline=int(time()) + 2,
                                                  with_gas=True, wait_for_receipt=True,
                                                  tokenIn=ETH, tokenOut=DAI,
                                                  amountIn=amount_in, amountOutMin=amount_out_min)
            uniswap_exchange_contract.call_method('readBalance')
            uniswap_exchange_contract.call_method('destroy', with_gas=True, wait_for_receipt=True)

        @staticmethod
        def test_flashloan_smart_contract():
            aave_flashloan_contract = ContractApi(ConfigRopsten.ContractPath.AAVE_FLASHLOAN)
            aave_flashloan_contract.deploy(provided_address=ConfigRopsten.EthereumAddress.AAVE_ADDRESS_LENDING_POOL,
                                           deposit_on_deploy=1000000000000000)
            logger.log("Waiting 2 seconds for contracts to finish deploying....")
            sleep(2)

            aave_flashloan_contract.call_method('readBalance')
            # uniswap_exchange_contract.call_method('readBalance')

            # uniswap_exchange_contract.call_method('getFactory')
            aave_flashloan_contract.call_method('takeFlashLoan', wait_for_receipt=True,
                                                with_gas=True, amountWei=1000000000000000)

            logger.log("Waiting for block to end (20 secs).....")
            sleep(20)

            aave_flashloan_contract.call_method('readBalance')
            # uniswap_exchange_contract.call_method('readBalance')

            aave_flashloan_contract.call_method('readLog')
            aave_flashloan_contract.call_method('destroy', with_gas=True, wait_for_receipt=True)
            # uniswap_exchange_contract.call_method('destroy', with_gas=True, wait_for_receipt=True)

    class TestArbitrageManager:
        @staticmethod
        def test_run_arbitrage():
            arbitrage_manager = ArbitrageManager()
            logger.log(arbitrage_manager.exchanges[0].get_balance())

            for i in range(10):  # In production, will run under while True
                arbitrage = arbitrage_manager.search_for_arbitrage()
                if arbitrage:
                    logger.log(arbitrage)  # In production, implementing-arbitrage-logic goes here
                logger.log("sleeping... for 2 seconds")
                sleep(2)


# Test.TestArbitrageManager.test_run_arbitrage()
# Test.TestSmartContracts.test_uniswap_smart_contract()
# Test.TestSmartContracts.test_flashloan_smart_contract()
