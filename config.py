class ConfigRopsten:
    class Markets:
        CURRENCIES = ["DAI", "ETH"]
        API_KEYS = {"binance": {"apiKey": "1MpAPuJly3Ywmavs5OSO7gtCefMRP9IK4B7EeivYdlMUD5Sxg1K29XU700xsRjLB",
                                "secret": "2tskgwQoitWTkWX7BH7IzYfu3gyZdlvUSYBSZoiLgcQhBp0v6SDbmjDcZdb7MHhF"}}

    class EthereumAddress:
        ACCOUNT_PRIVATE_KEY = 'f522d599943a7ca323ffaa7850793721ecf53b5248a800651d513198ea8fea4a'

        INFURA_HTTP_NODE = "https://ropsten.infura.io/v3/5b11428a172c42f3964cbd333a587c5d"

        AAVE_ADDRESS_LENDING_POOL = '0x1c8756FD2B28e9426CDBDcC7E3c4d64fa9A54728'

        UNISWAP_ADDRESS_ROUTER = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
        UNISWAP_ADDRESS_ETH = "0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE"
        UNISWAP_ADDRESS_DAI = "0xf80a32a835f79d7787e8a8ee5721d0feafd78108"

    class ContractPath:
        AAVE_FLASHLOAN = 'src/contracts/aave_flashloan.sol'
        UNISWAP_EXCHANGE = 'src/contracts/uniswap_api.sol'
