from config import ConfigRopsten
from src.logger import Logger

from web3 import Web3
from solcx import compile_standard, set_solc_version_pragma

import json


class ContractApi:
    """ This class is an easy API for compiling, deploying and interacting with a .sol smart contract """
    def __init__(self, contract_path: str):
        self.logger = Logger()
        self.w3 = Web3(Web3.HTTPProvider(ConfigRopsten.EthereumAddress.INFURA_HTTP_NODE))

        self.contract_file_name = contract_path.split('/')[-1]
        self.contract_name = None
        self.contract = self._create_contract_object(contract_path)

        self.sender_account = self.w3.eth.account.privateKeyToAccount(ConfigRopsten.EthereumAddress.ACCOUNT_PRIVATE_KEY)

    def _create_contract_object(self, contract_path):
        """ Compile the .sol file located at 'contract_path' to create a contract-object """

        def compile_sol_file():
            with open(contract_path, 'r') as solidity_contract_file:
                sol_contract = solidity_contract_file.read()

            required_pragma_version = sol_contract.split(';')[0].split('solidity ')[1]
            self.logger.log(f"Compiling contract {self.contract_file_name} (solidity {required_pragma_version})")
            set_solc_version_pragma(required_pragma_version)

            return compile_standard(
                {"language": "Solidity", "sources": {self.contract_file_name: {"content": sol_contract}},
                 "settings": {"outputSelection": {"*": {"*": ["metadata", "evm.bytecode", "evm.bytecode.sourceMap"]}}}},
                allow_paths=".")

        compiled_sol = compile_sol_file()
        self.contract_name = list(compiled_sol['contracts'][self.contract_file_name].keys())[0]
        compiled_contract = compiled_sol['contracts'][self.contract_file_name][self.contract_name]

        bytecode = compiled_contract['evm']['bytecode']['object']
        abi = json.loads(compiled_contract['metadata'])['output']['abi']

        return self.w3.eth.contract(abi=abi, bytecode=bytecode)

    def _create_transaction_data(self, with_gas: bool = False, value: int = 0):
        """ Creates a valid transaction-dictionary """
        tx_data = {'from': self.sender_account.address}

        if with_gas:
            gas_limit = self.w3.eth.getBlock('latest').gasLimit
            tx_data.update({'gas': gas_limit, 'gasPrice': self.w3.toWei('21', 'gwei'), 'value': value,
                            'nonce': self.w3.eth.getTransactionCount(self.sender_account.address)})
        return tx_data

    def _send_transaction(self, tx, wait_for_receipt: bool):
        """ Signed the transaction object, sends it, and return the transaction result (hash/receipt) """
        tx_signed = self.sender_account.sign_transaction(tx)
        tx_hash = self.w3.eth.sendRawTransaction(tx_signed.rawTransaction)
        return tx_hash if not wait_for_receipt else self.w3.eth.waitForTransactionReceipt(tx_hash)

    def create_valid_address(self, address):
        """ Convert address to a valid checkSum address """
        return self.w3.toChecksumAddress(address)

    def deploy(self, provided_address: str, deposit_on_deploy: int = 0) -> str:
        """ Deploy the contract by calling its constructor method (create a transaction to it) """
        valid_address = self.create_valid_address(provided_address)

        tx_data = self._create_transaction_data(with_gas=True, value=deposit_on_deploy)
        tx = self.contract.constructor(valid_address).buildTransaction(tx_data)

        self.logger.log(f"Deploying contract {self.contract_name}...")
        tx_receipt = self._send_transaction(tx, wait_for_receipt=True)

        self.contract.address = tx_receipt.contractAddress
        return tx_receipt.contractAddress

    def get_methods(self) -> list:
        """ Returns a list of strings naming all contract's ABI methods """
        return [method.fn_name for method in self.contract.all_functions()]

    def call_method(self, method_name: str, with_gas: bool = False, wait_for_receipt: bool = False,
                    value: int = 0, **method_params):
        """ Interact with contract's methods """
        self.logger.log(f"Interacting with '{self.contract_name}' - method '{method_name}' GAS: {with_gas}")
        tx_data = self._create_transaction_data(with_gas=with_gas, value=value)

        if with_gas:
            tx = self.contract.get_function_by_name(method_name)(**method_params).buildTransaction(tx_data)
            tx_result = self._send_transaction(tx, wait_for_receipt=wait_for_receipt)
            result_type = "receipt" if wait_for_receipt else "hash (not waiting for a receipt)"
            self.logger.log(f"Transaction {result_type}: {tx_result}\n\n")
            return tx_result

        else:
            response = self.contract.get_function_by_name(method_name)(**method_params).call(tx_data)
            self.logger.log(f"Response from contract: {response}\n\n")
            return response
