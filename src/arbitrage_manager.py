from config import ConfigRopsten
from src.logger import Logger
from src.ccxt_exchange import CcxtExchange


class ArbitrageManager:
    """ This class is in charge of finding arbitrages in the markets defined by the config file """
    def __init__(self):
        self.logger = Logger()
        self.exchanges = [CcxtExchange(exchange_name) for exchange_name in ConfigRopsten.Markets.API_KEYS]

    def _fetch_exchanges_prices(self) -> list:
        """ Get all prices of relevant markets in all exchanges """
        return [exchange.get_prices() for exchange in self.exchanges]

    def _is_markets_inverse(self, first: dict, second: dict) -> bool:
        """ Check if two markets are inverses of one another """
        return first["base"] == second["quote"] and first["quote"] == second["base"]

    def _is_arbitrage(self, first: dict, second:dict) -> bool:
        """ Check if markets' prices create an arbitrage """
        return first['price'] > 1 / second["price"]

    def _create_arb_message(self, first: dict, second: dict, profit: float) -> str:
        """ Create a message that announces that an arbitrage was found """
        return f"\n!!! POSSIBLE ARBITRAGE !!! Expected profit: {'%.2f' % profit} % !!!\n" \
               f"{first['name']}: {first['base']}/{first['quote']} ({first['price']}) --- " \
               f"{second['name']}: {second['base']}/{second['quote']} ({second['price']})\n"

    def search_for_arbitrage(self):
        """ Search for an arbitrage and return the first one found. If none were found, return None """
        self.logger.log(f"Searching for arbitrages in {[exchange.exchange.name for exchange in self.exchanges]}")
        exchanges_prices = self._fetch_exchanges_prices()

        for i in range(len(exchanges_prices)):
            for j in range(i+1, len(exchanges_prices)):
                first, second = exchanges_prices[i], exchanges_prices[j]
                if self._is_markets_inverse(first, second) and self._is_arbitrage(first, second):
                    profit = (first['price'] * second['price'] - 1) * 100
                    self.logger.log(self._create_arb_message(first, second, profit))
                    return {"ex1": first['name'], "bs1": first['base'], "qt1": first['quote'], "ex2": second['name']}
