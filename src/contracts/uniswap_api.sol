pragma solidity ^0.6.6;

import "/home/omri/runarbitrage/src/contracts/uniswap/uniswap-v2-periphery/interfaces/IUniswapV2Router02.sol";

contract uniswapApi {
    address payable creatorAddress;
    IUniswapV2Router02 uniswapRouter;

    constructor(address _addressProvider) public payable{
        creatorAddress = msg.sender ;
        uniswapRouter = IUniswapV2Router02(_addressProvider);
    }

    function readBalance() public view returns (uint){
        return address(this).balance ;
    }

    function destroy() public {
        if (msg.sender == creatorAddress){
            selfdestruct(creatorAddress) ;
        }
    }

    function getFactory() public returns (address){
        return uniswapRouter.factory();
    }

    function buyTokens(address tokenIn, address tokenOut,
                       uint amountIn, uint amountOutMin, uint deadline)
                       public payable returns (uint[] memory amounts){
        address[] memory path = new address[](2);
        path[0] = tokenIn ;
        path[1] = tokenOut ;
        return uniswapRouter.swapExactTokensForTokens(amountIn, amountOutMin, path, address(this), deadline) ;
    }
}
