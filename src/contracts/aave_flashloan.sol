pragma solidity ^0.6.6;

import "/home/omri/runarbitrage/src/contracts/aave/aave/FlashLoanReceiverBase.sol";
import "/home/omri/runarbitrage/src/contracts/aave/aave/ILendingPool.sol";
import "/home/omri/runarbitrage/src/contracts/aave/aave/ILendingPoolAddressesProvider.sol";

contract AaveFlashLoan is FlashLoanReceiverBase {
    address payable creatorAddress;
    string log;

    constructor(address _addressProvider) FlashLoanReceiverBase(_addressProvider) public payable{
        creatorAddress = msg.sender ;
        log = "\n====== Log ======";
    }

    function readLog() public view returns (string memory){
        return log;
    }

    function writeLog(string memory logEntry) private {
        log = string(abi.encodePacked(log, '\n\n', logEntry)) ;
    }

    function readBalance() public view returns (uint){
        return address(this).balance ;
    }

    function destroy() public {
        if (msg.sender == creatorAddress){
            selfdestruct(creatorAddress) ;
        }
    }

    function executeOperation(
        address _reserve, uint256 _amount, uint256 _fee, bytes calldata _params) external override {
        require(_amount <= getBalanceInternal(address(this), _reserve), "Invalid balance, was the flashLoan successful?");

        writeLog('Got Flashloan!');

        // ============================ //
        // === Implement some logic === //
        // ============================ //

        uint totalDebt = _amount.add(_fee);

        writeLog('returning Flashloan...');

        transferFundsBackToPoolInternal(_reserve, totalDebt);
    }

    function takeFlashLoan(uint256 amountWei) public onlyOwner {
        address receiver = address(this) ;
        address asset = 0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE;
        bytes memory params = "";

        ILendingPool lendingPool = ILendingPool(addressesProvider.getLendingPool());
        lendingPool.flashLoan(receiver, asset, amountWei, params);
    }
}
