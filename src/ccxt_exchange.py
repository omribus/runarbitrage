from config import ConfigRopsten
from src.logger import Logger

import ccxt
# from web3 import Web3


class CcxtExchange:
    """ This class provides an easy API to communicate with exchanges """
    def __init__(self, exchange_name: str):
        self.logger = Logger()
        self.logger.log(f"Initiating exchange {exchange_name}...")
        self.exchange = getattr(ccxt, exchange_name)(ConfigRopsten.Markets.API_KEYS[exchange_name])
        self.market_symbols = [market['symbol'] for market in self.exchange.fetch_markets() if
                               self._is_market_relevant(market['symbol'])]

        self.logger.log(f"{self.exchange} was initiated with markets: {self.market_symbols}\n")

    @staticmethod
    def _is_market_relevant(market_symbol: str) -> bool:
        """ Check if market handles the desired currencies """
        base, quote = market_symbol.split('/')
        return base in ConfigRopsten.Markets.CURRENCIES and quote in ConfigRopsten.Markets.CURRENCIES

    def get_balance(self):
        """ Get a 'Balance' object representing the current balance of the exchange account """
        return self.exchange.fetch_balance()

    # TODO: Implement properly 'deposit' method into exchange account
    # def deposit(self, coin_code: str, amount: int):
    #     """ Deposit 'amount' of 'coin_code' into the exchange's account """
    #     deposit_address = self.exchange.fetchDepositAddress(coin_code)['address']
    #     self.logger.log(f"---Should deposit {amount} {coin_code} to---\n{deposit_address}")
    #
    #     w3 = Web3(Web3.HTTPProvider(ConfigRopsten.Ethereum.INFURA_HTTP_NODE))
    #     sender_account = w3.eth.account.privateKeyToAccount(ConfigRopsten.Ethereum.ACCOUNT_PRIVATE_KEY)
    #
    #     tx_data = {'from': sender_account.address, 'to': w3.toChecksumAddress(deposit_address),
    #                'gas': 10000000, 'gasPrice': w3.toWei('21', 'gwei'),
    #                'value': amount, 'nonce': w3.eth.getTransactionCount(sender_account.address)}
    #     w3.eth.signTransaction()
    #     tx_hash = w3.eth.sendTransaction(tx_data)
    #     tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)
    #     return tx_receipt

    def get_prices(self) -> dict:
        """ Return ask-prices of relevant markets in the exchange """
        self.logger.log(f"Fetching prices for {self.exchange.name} ({self.market_symbols})")
        prices = []

        for market_symbol in self.market_symbols:
            base, quote = market_symbol.split('/')
            price = self.exchange.fetch_ticker(market_symbol)['ask']

            prices.append({"name": self.exchange.name, "base": base, "quote": quote, "price": price})

        return prices[0]
